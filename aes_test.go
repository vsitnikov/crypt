package crypt

import (
	"github.com/stretchr/testify/require"
	"testing"
)

type dataTestAes struct {
	source   string
	password string
	result   string
}

var resultsTestAes []dataTestAes

func TestEncryptWithPass(t *testing.T) {
	resultsTestAes = make([]dataTestAes, 0)
	words := []string{"1", "два", "four", "eight123", "sixteen!sixteen!", "thirty two thirty two thirty two", "Тревожные вести с Гнипахеллира, мой тан."}

	for _, value := range words {
		if result, err := EncryptWithPass(value, "MyPassword"); err != nil {
			t.Fatalf("Fail encrypt, reason: %s", err)
		} else {
			resultsTestAes = append(resultsTestAes, dataTestAes{
				source:   value,
				password: "MyPassword",
				result:   result,
			})
		}
	}
}

func TestDecryptWithPassPanic(t *testing.T) {
	defer func() {
		if r := recover(); r == nil {
			t.Errorf("Panic test failed")
		}
	}()

	if _, err := DecryptWithPass("At2WsklP4Id4aLC+hAN78UOMD8uLrgzAvNkfH5vLCGr3+TcxizgU2bcde+vGvgz5aNYMQiduRJ6Ybj9W4uvB", "two"); err == nil {
		t.Fatalf("Panic test failed")
	}
}

func TestDecryptWithPass(t *testing.T) {
	TestEncryptWithPass(t)
	if _, err := DecryptWithPass("one", "two"); err == nil {
		t.Fatalf("Incorrect crypted data test failed")
	}
	if _, err := DecryptWithPass("8m+xz5PqAe2nl0nO4bnXgtY9mjCC3UXR/g3u/CWMp54RImoyidfp+C4PIEVJabDaMidONVKI73QOma4Zk2nDdg==", "two"); err == nil {
		t.Fatalf("Incorrect hmac test failed")
	}
	for _, value := range resultsTestAes {
		if result, err := DecryptWithPass(value.result, value.password); err != nil {
			t.Fatalf("Fail decrypt for %s, reason: %s", value.source, err)
		} else {
			require.Equal(t, result, value.source, "Incorrect crypt/decrypt: waiting %s, getting %s", value.source, result)
		}
	}
}

func TestPad(t *testing.T) {
	data := make([]byte, 10)
	result := Pad(data, 32)
	require.Equal(t, len(result), 32, "Incorrect padding: waiting length %s, getting %s", 32, result)
}

func TestUnpad(t *testing.T) {
	data := make([]byte, 10)
	if _, err := Unpad(data, 32); err == nil {
		t.Fatalf("Fail unpad")
	}
}
