package crypt

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"fmt"
	"os"
)

// GenerateKeyPair generates a new key pair
func GenerateKeyPair(bits int) (*rsa.PrivateKey, *rsa.PublicKey) {
	//	Never error, when use rand.Reader
	privateKey, _ := rsa.GenerateKey(rand.Reader, bits)
	return privateKey, &privateKey.PublicKey
}

// PrivateKeyToBytes private key to bytes
func PrivateKeyToBytes(privateKey *rsa.PrivateKey) []byte {
	privateBytes := pem.EncodeToMemory(
		&pem.Block{
			Type:  "RSA PRIVATE KEY",
			Bytes: x509.MarshalPKCS1PrivateKey(privateKey),
		},
	)

	return privateBytes
}

// PublicKeyToBytes public key to bytes
func PublicKeyToBytes(pub *rsa.PublicKey) []byte {

	//	If key types are *rsa.PublicKey don't error
	pubASN1, _ := x509.MarshalPKIXPublicKey(pub)

	pubBytes := pem.EncodeToMemory(&pem.Block{
		Type:  "PUBLIC KEY",
		Bytes: pubASN1,
	})

	return pubBytes
}

func PrivateKeyFromFile(filename string) (*rsa.PrivateKey, error) {
	//	Read file with private key
	PrivateKeyFile, err := os.ReadFile(filename)
	if err != nil {
		PrivateKeyFile = []byte(filename)
	}
	privatePemKey, _ := pem.Decode(PrivateKeyFile)
	if privatePemKey == nil {
		if err != nil {
			return nil, errors.New("unable to read private key: " + err.Error())
		}
		return nil, errors.New("bad key dataTestAes: not PEM-encoded")
	}
	if got, want := privatePemKey.Type, "RSA PRIVATE KEY"; got != want {
		return nil, errors.New(fmt.Sprintf("unknown key type %q, want %q", got, want))
	}

	// Decode the RSA private key
	privateKey, err := x509.ParsePKCS1PrivateKey(privatePemKey.Bytes)
	if err != nil {
		return nil, errors.New("bad private key: " + err.Error())
	}

	return privateKey, nil
}

func PublicKeyFromFile(filename string) (*rsa.PublicKey, error) {

	//	Read file with public key
	publicKeyFile, err := os.ReadFile(filename)
	if err != nil {
		publicKeyFile = []byte(filename)
	}
	publicPemKey, _ := pem.Decode(publicKeyFile)
	if publicPemKey == nil {
		if err != nil {
			return nil, errors.New("unable to read public key: " + err.Error())
		}
		return nil, errors.New("bad key dataTestAes: not PEM-encoded")
	}
	if got, want := publicPemKey.Type, "PUBLIC KEY"; got != want {
		return nil, errors.New(fmt.Sprintf("unknown key type %q, want %q", got, want))
	}

	// Decode the RSA public key
	publicInterface, err := x509.ParsePKIXPublicKey(publicPemKey.Bytes)
	if err != nil {
		return nil, errors.New("bad public key: " + err.Error())
	}
	publicKey := publicInterface.(*rsa.PublicKey)

	return publicKey, nil
}

func EncryptWithPublicKey(plainText []byte, publicKey *rsa.PublicKey) ([]byte, error) {
	cryptText, err := rsa.EncryptPKCS1v15(rand.Reader, publicKey, plainText)
	if err != nil {
		return nil, errors.New("encrypt: " + err.Error())
	}
	return cryptText, nil
}

func EncryptWithPublicKeyFile(plainText []byte, filename string) ([]byte, error) {
	publicKey, err := PublicKeyFromFile(filename)
	if err != nil {
		return nil, err
	}
	return EncryptWithPublicKey(plainText, publicKey)
}

func DecryptWithPrivateKey(encryptedText []byte, privateKey *rsa.PrivateKey) ([]byte, error) {
	cipherKey, err := rsa.DecryptPKCS1v15(rand.Reader, privateKey, encryptedText)
	if err != nil {
		return nil, errors.New("decrypt: " + err.Error())
	}
	return cipherKey, nil
}

func DecryptWithPrivateKeyFile(encryptedText []byte, filename string) ([]byte, error) {
	privateKey, err := PrivateKeyFromFile(filename)
	if err != nil {
		return nil, err
	}
	return DecryptWithPrivateKey(encryptedText, privateKey)
}
