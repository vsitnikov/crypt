package crypt

import (
	"crypto/rsa"
	"github.com/stretchr/testify/require"
	"strings"
	"testing"
)

type rsaKeys struct {
	privateKey       *rsa.PrivateKey
	publicKey        *rsa.PublicKey
	privateKeyString string
	publicKeyString  string
}

type dataTestRsa struct {
	source  string
	private string
	public  string
	result  []byte
}

var keys rsaKeys
var resultsTestRsa []dataTestRsa

func TestGenerateKeyPair(t *testing.T) {
	require.Empty(t, keys.privateKey, "Initialize failed: variable not empty")
	keys.privateKey, keys.publicKey = GenerateKeyPair(4096)
	require.NotEmpty(t, keys.privateKey, "Private key failed: key is empty")
	require.NotEmpty(t, keys.publicKey, "Public key failed: key is empty")
}

func TestPrivateKeyToBytes(t *testing.T) {
	if keys.privateKey == nil {
		TestGenerateKeyPair(t)
	}
	keys.privateKeyString = string(PrivateKeyToBytes(keys.privateKey))
	require.Contains(t, keys.privateKeyString, "-----BEGIN RSA PRIVATE KEY-----", "Invalid RSA private key: no begin marker")
	require.Contains(t, keys.privateKeyString, "-----END RSA PRIVATE KEY-----", "Invalid RSA private key: no end marker")
}

func TestPrivateKeyFromFile(t *testing.T) {
	if keys.privateKey == nil || keys.privateKeyString == "" {
		TestPrivateKeyToBytes(t)
	}

	//	Test invalid filename in private key
	if _, err := PrivateKeyFromFile("error private key"); err == nil {
		t.Fatalf("Fail detect invalid private key file")
	}

	//	Test invalid content in private key
	if _, err := PrivateKeyFromFile("rsa_test.go"); err == nil {
		t.Fatalf("Fail detect invalid content in private key")
	}

	//	Test invalid private key type
	if _, err := PrivateKeyFromFile(strings.Replace(keys.privateKeyString, "RSA PRIVATE KEY", "PRIVATE KEY", -1)); err == nil {
		t.Fatalf("Fail detect invalid private key type")
	}

	//	Test modified private key
	if _, err := PrivateKeyFromFile(strings.Replace(keys.privateKeyString, "0", "R", -1)); err == nil {
		t.Fatalf("Fail detect modified private key")
	}

	if privateKey, err := PrivateKeyFromFile(keys.privateKeyString); err != nil {
		t.Fatalf("Invalid private key conversion, reason: %s", err)
	} else {
		require.Equal(t, privateKey, keys.privateKey, "Failed to convert private key")
	}
}

func TestPublicKeyToBytes(t *testing.T) {
	if keys.publicKey == nil {
		TestGenerateKeyPair(t)
	}
	publicKey := PublicKeyToBytes(keys.publicKey)
	keys.publicKeyString = string(publicKey)
	require.Contains(t, keys.publicKeyString, "-----BEGIN PUBLIC KEY-----", "Invalid public key: no begin marker")
	require.Contains(t, keys.publicKeyString, "-----END PUBLIC KEY-----", "Invalid public key: no end marker")
}

func TestPublicKeyFromFile(t *testing.T) {
	if keys.publicKey == nil || keys.publicKeyString == "" {
		TestPublicKeyToBytes(t)
	}

	//	Test invalid filename in public key
	if _, err := PublicKeyFromFile("error public key"); err == nil {
		t.Fatalf("Fail detect invalid public key file")
	}

	//	Test invalid content in public key
	if _, err := PublicKeyFromFile("rsa_test.go"); err == nil {
		t.Fatalf("Fail detect invalid content in public key")
	}

	//	Test invalid public key type
	if _, err := PublicKeyFromFile(strings.Replace(keys.publicKeyString, "PUBLIC KEY", "PUBLIC_KEY", -1)); err == nil {
		t.Fatalf("Fail detect invalid public key type")
	}

	//	Test modified public key
	if _, err := PublicKeyFromFile(strings.Replace(keys.publicKeyString, "0", "R", -1)); err == nil {
		t.Fatalf("Fail detect modified public key")
	}

	if publicKey, err := PublicKeyFromFile(keys.publicKeyString); err != nil {
		t.Fatalf("Invalid public key conversion, reason: %s", err)
	} else {
		require.Equal(t, publicKey, keys.publicKey, "Failed to convert public key")
	}
}

func TestEncryptWithPublicKey(t *testing.T) {
	resultsTestRsa = make([]dataTestRsa, 0)
	privateKey, publicKey := GenerateKeyPair(4096)
	privateKeyString := string(PrivateKeyToBytes(privateKey))
	publicKeyBytes := PublicKeyToBytes(publicKey)

	_, failPublicKey := GenerateKeyPair(32)
	failPublicKey.E++
	if _, err := EncryptWithPublicKey([]byte("test failed"), failPublicKey); err == nil {
		t.Fatalf("Fail detect invalid public key")
	}

	words := []string{"1", "два", "four", "eight123", "sixteen!sixteen!", "thirty two thirty two thirty two", "Тревожные вести с Гнипахеллира, мой тан."}
	for _, value := range words {
		if result, err := EncryptWithPublicKey([]byte(value), publicKey); err != nil {
			t.Fatalf("Fail encrypt, reason: %s", err)
		} else {
			resultsTestRsa = append(resultsTestRsa, dataTestRsa{
				source:  value,
				private: privateKeyString,
				public:  string(publicKeyBytes),
				result:  result,
			})
		}
	}
}

func TestDecryptWithPrivateKey(t *testing.T) {
	TestEncryptWithPublicKey(t)
	privateKey, _ := PrivateKeyFromFile(resultsTestRsa[0].private)
	if _, err := DecryptWithPrivateKey([]byte("fail"), privateKey); err == nil {
		t.Fatalf("Fail decrypt with private key")
	}
	for _, value := range resultsTestRsa {
		privateKey, _ := PrivateKeyFromFile(value.private)
		if result, err := DecryptWithPrivateKey(value.result, privateKey); err != nil {
			t.Fatalf("Fail decrypt for %s, reason: %s", value.source, err)
		} else {
			require.Equal(t, string(result), value.source, "Incorrect crypt/decrypt: waiting %s, getting %s", value.source, result)
		}
	}
}

func TestEncryptWithPublicKeyFile(t *testing.T) {

	//	Detect invalid public key file
	if _, err := EncryptWithPublicKeyFile([]byte("fail"), "fail key"); err == nil {
		t.Fatalf("Fail detect invalid encrypt")
	}

	resultsTestRsa = make([]dataTestRsa, 0)
	privateKey, publicKey := GenerateKeyPair(4096)
	privateKeyString := string(PrivateKeyToBytes(privateKey))
	publicKeyBytes := PublicKeyToBytes(publicKey)

	words := []string{"1", "два", "four", "eight123", "sixteen!sixteen!", "thirty two thirty two thirty two", "Тревожные вести с Гнипахеллира, мой тан."}
	for _, value := range words {
		if result, err := EncryptWithPublicKeyFile([]byte(value), string(publicKeyBytes)); err != nil {
			t.Fatalf("Fail encrypt, reason: %s", err)
		} else {
			resultsTestRsa = append(resultsTestRsa, dataTestRsa{
				source:  value,
				private: privateKeyString,
				public:  string(publicKeyBytes),
				result:  result,
			})
		}
	}
}

func TestDecryptWithPrivateKeyFile(t *testing.T) {

	//	Detect invalid private key file
	if _, err := DecryptWithPrivateKeyFile([]byte("fail"), "fail key"); err == nil {
		t.Fatalf("Fail detect invalid decrypt")
	}

	TestEncryptWithPublicKeyFile(t)
	for _, value := range resultsTestRsa {
		if result, err := DecryptWithPrivateKeyFile(value.result, value.private); err != nil {
			t.Fatalf("Fail decrypt for %s, reason: %s", value.source, err)
		} else {
			require.Equal(t, string(result), value.source, "Incorrect crypt/decrypt: waiting %s, getting %s", value.source, result)
		}
	}
}
